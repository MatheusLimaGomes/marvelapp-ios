//
//  CharactersPresenter.swift
//  MarvelAppSwift
//
//  Created by Matheus Lima Gomes on 21/03/20.
//  Copyright © 2020 Matheus Lima Gomes. All rights reserved.
//

import Foundation
protocol CharactersListPresentationLogic {
    func presentCharacter(response: Response)
}
class CharactersListPresenter: CharactersListPresentationLogic {
    
    var viewController: CharactersListTableViewController?
    func presentCharacter(response: Response) {
        var displayCharacters: [CharactersModel.FetchCharacters.ViewModel.Character] = []
        let characters = response.characters.compactMap { (characters) -> CharactersModel.FetchCharacters.ViewModel.Character in
            let charactersModel = CharactersModel.FetchCharacters.ViewModel.self
             return  charactersModel.Character(id: characters.id, name: characters.name, description: characters.description, thumbnail: characters.thumbnail)
        }
        displayCharacters.append(contentsOf: characters)
        
        let viewModel = CharactersModel.FetchCharacters.ViewModel(characters: displayCharacters)
        viewController?.displayFetchCharacters(viewModel: viewModel)
        
    }
}
