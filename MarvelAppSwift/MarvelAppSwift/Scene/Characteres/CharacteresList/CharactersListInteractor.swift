//
//  HeroesListInteractor.swift
//  MarvelAppSwift
//
//  Created by Matheus Lima Gomes on 21/03/20.
//  Copyright © 2020 Matheus Lima Gomes. All rights reserved.
//

import Foundation
protocol CharactersBusinessLogic {
    func fetchCharacters(request: CharactersModel.FetchCharacters.Request)
}
class CharactersListInteractor: CharactersBusinessLogic {
    var presenter: CharactersListPresenter?
    
    func fetchCharacters(request: CharactersModel.FetchCharacters.Request) {
        
    }
    
    
    
}
