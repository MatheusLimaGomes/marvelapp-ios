//
//  CharactersListTableViewController.swift
//  MarvelAppSwift
//
//  Created by Matheus Lima Gomes on 21/03/20.
//  Copyright © 2020 Matheus Lima Gomes. All rights reserved.
//

import UIKit
protocol CharactersDisplayLogic {
    func displayFetchCharacters(viewModel: CharactersModel.FetchCharacters.ViewModel)
}
class CharactersListTableViewController: UITableViewController, CharactersDisplayLogic {
    
    private var characters: [Character] = []
    
    var displayadCharacters: [CharactersModel
        .FetchCharacters.ViewModel.Character] = []
    
    var displayFetchedCharacters: CharactersModel.FetchCharacters.ViewModel?
    
    var interactor: CharactersListInteractor?
    private var offset = 20
    
    func displayFetchCharacters(viewModel: CharactersModel.FetchCharacters.ViewModel) {
        self.displayadCharacters = viewModel.characters
    }
    
    var tableview = UITableView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        startLoading()
        setup()
    }
    override func viewWillAppear(_ animated: Bool) {
        tableView.accessibilityIdentifier = "CharactersTableView"
        fetchCharacters()
    }
    
    func setup() {
        setupTableview()
    }
    
    private func setupTableview() {
        let viewController = self
        let presenter = CharactersListPresenter()
        interactor = CharactersListInteractor()
        interactor?.presenter = presenter
        presenter.viewController = viewController
        view.addSubview(tableview)
        tableView.register(CharacterTableViewCell.self, forCellReuseIdentifier: "characterCell")
        self.tableview
            .setupTableViewConstraints(view: self.view)
        fetchCharacters()
        self.tableView.reloadData()
    }
    
    private func fetchCharacters() {
        let request = CharactersModel.FetchCharacters.Request(offset: offset, charactersRequest: characters)
        interactor?.fetchCharacters(request: request)
    }
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return displayadCharacters.count
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return displayadCharacters.count
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell: CharacterTableViewCell = tableView
            .dequeueReusableCell(withIdentifier: "characterCell",
                                 for: indexPath) as? CharacterTableViewCell else {
                                    return  UITableViewCell()
        }
        cell.character = self.displayadCharacters[indexPath.row]
        return cell
    }
    
    override func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if (scrollView.contentOffset.y + scrollView
            .frame.size.height) >= scrollView
            .contentSize.height {
            offset = offset + 20
            fetchCharacters()
        }
    }
}
