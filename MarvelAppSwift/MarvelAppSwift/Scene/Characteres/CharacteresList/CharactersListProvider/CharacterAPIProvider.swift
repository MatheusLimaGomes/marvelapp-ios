//
//  CharacterAPIProvider.swift
//  MarvelAppSwift
//
//  Created by Matheus Lima Gomes on 24/03/20.
//  Copyright © 2020 Matheus Lima Gomes. All rights reserved.
//

import Foundation

class CharacterApiProvider {
    static func fetchCharacter(url: URL, completionHandler: @escaping CharactersCallBack) {
        
        let task = MarvelApiProvider.session.dataTask(with: url) { (data,
            response,
            error) in
            if error != nil {
                completionHandler(.failure(error:
                    .cannotFetch(error?.localizedDescription ?? "Unknown error")))
            }
            guard let response = response as? HTTPURLResponse else {
                completionHandler(.failure(error:
                    .cannotFetch("Response error")))
                return
            }
            
            if response.statusCode != 200 {
                completionHandler(.failure(error:
                    .cannotFetch("Status code \(response.statusCode)")))
            }
            
            guard let data = data else {
                completionHandler(.failure(error:
                    .cannotFetch("Error in response data")))
                return
                
            }
            do {
                let characters = try JSONDecoder().decode([Character].self, from: data)
                completionHandler(.success(result: characters))
            } catch {
                completionHandler(.failure(error: .cannotFetch("Error parse Realty")))
            }
        }
        task.resume()
    }
}
