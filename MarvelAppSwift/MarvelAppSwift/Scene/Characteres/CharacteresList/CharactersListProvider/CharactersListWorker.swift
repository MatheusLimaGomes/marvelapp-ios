//
//  CharactersListWorker.swift
//  MarvelAppSwift
//
//  Created by Matheus Lima Gomes on 22/03/20.
//  Copyright © 2020 Matheus Lima Gomes. All rights reserved.
//

import Foundation
typealias CharactersCallBack = (Result<[Character]>) -> Void


protocol CharactersProtocol {
    func fetchCharactersList(completionHandler: @escaping CharactersCallBack)
}

enum Result<T> {
    case success(result: T)
    case failure(error: CharacterOptionStoreError)
}

enum CharacterOptionStoreError: Error {
    case cannotFetch(String)
}
class CharacterWorker {
    var characterProtocol: CharactersProtocol
    init(charactersProtocol: CharactersProtocol) {
        self.characterProtocol = charactersProtocol
    }
    
    func fetchCharactersList(completionHandler: @escaping CharactersCallBack) {
        characterProtocol.fetchCharactersList { (result) in
            completionHandler(result)
        }
    }
}
