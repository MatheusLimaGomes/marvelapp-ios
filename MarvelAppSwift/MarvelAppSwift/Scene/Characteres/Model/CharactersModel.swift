//
//  CharactersModel.swift
//  MarvelAppSwift
//
//  Created by Matheus Lima Gomes on 21/03/20.
//  Copyright © 2020 Matheus Lima Gomes. All rights reserved.
//

import Foundation

enum CharactersModel {
    struct FetchCharacters {
        struct Request {
            var offset: Int
            var charactersRequest: [Character]
        }
        struct Response {
            var offset: Int
            var repositoriesResponse: [Character]
        }
        struct ViewModel {
            struct Character {
                let id: Int
                let name: String?
                let description: String?
                let thumbnail: String?
            }
            var characters: [Character]
        }
    }
}
