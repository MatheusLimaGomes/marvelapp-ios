//
//  CharacterTableViewCell.swift
//  MarvelAppSwift
//
//  Created by Matheus Lima Gomes on 21/03/20.
//  Copyright © 2020 Matheus Lima Gomes. All rights reserved.
//

import UIKit

class CharacterTableViewCell: UITableViewCell {
    
    var character : CharactersModel.FetchCharacters.ViewModel.Character? {
        didSet {
            setupUI()
        }
    }
    let characterProfileImageView: UIImageView = {
        let img = UIImageView()
        img.contentMode = .scaleAspectFit
        img.translatesAutoresizingMaskIntoConstraints = false
        img.layer.cornerRadius = 35
        img.clipsToBounds = true
        return img
    }()
    let characterNameLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 20)
        label.textColor = .white
        label.layer.cornerRadius = 5
        label.clipsToBounds = true
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    let characterDescriprionLabel: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 15)
        label.textColor = .white
        label.layer.cornerRadius = 5
        label.clipsToBounds = true
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    let containerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints  = false
        view.clipsToBounds = true
        return view
    }()
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
extension CharacterTableViewCell {
    private func addSubviews() {
        self.contentView.addSubview(characterProfileImageView)
        containerView.addSubview(characterNameLabel)
        containerView.addSubview(characterDescriprionLabel)
        self.contentView.addSubview(containerView)
    }
    private func setupConstraints() {
        characterProfileImageView.centerYAnchor.constraint(equalTo: self.contentView.centerYAnchor).isActive = true
        containerView.centerYAnchor.constraint(equalTo:self.contentView.centerYAnchor).isActive = true
        containerView.leadingAnchor.constraint(equalTo:self.characterProfileImageView.trailingAnchor, constant:10).isActive = true
        containerView.trailingAnchor.constraint(equalTo:self.contentView.trailingAnchor, constant:-10).isActive = true
        containerView.heightAnchor.constraint(equalTo: self.contentView.heightAnchor).isActive = true
    }
}
extension CharacterTableViewCell {
    override func prepareForReuse() {
        characterProfileImageView.image = UIImage()
        characterNameLabel.text = ""
        characterDescriprionLabel.text = ""
    }
    private func setupUI() {
        if let char = character {
            guard let thumbnail = char.thumbnail else { return }
            characterProfileImageView.loadImage(withURL: URL(string: thumbnail))
            characterNameLabel.text = char.name ?? ""
            characterDescriprionLabel.text = char.description ?? "No Description"
        } else {
            prepareForReuse()
        }
    }
}
