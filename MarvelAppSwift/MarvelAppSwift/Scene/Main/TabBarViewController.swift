//
//  TabBarViewController.swift
//  MarvelAppSwift
//
//  Created by Matheus Lima Gomes on 21/03/20.
//  Copyright © 2020 Matheus Lima Gomes. All rights reserved.
//

import UIKit

class TabBarViewController: UITabBarController {
    private var charactersVC = CharactersListTableViewController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureNavigationControllers()
        setupTabBar()
        charactersVC.setup()
    }
    
    func configureNavigationControllers() {
        configureCharactersVC()
    }
    private func configureCharactersVC() {
        charactersVC.tabBarItem = UITabBarItem(title: "Heroes",
        image: UIImage(named:"walletOutline"),
        selectedImage:UIImage(named:"walletFilled"))
        
        let navigationController =  UINavigationController(rootViewController: charactersVC)
        viewControllers?.append(navigationController)
    }
    
    private func setupTabBar() {
        tabBar.barTintColor = UIColor.red
        tabBar.tintColor = .white
        tabBar.unselectedItemTintColor = UIColor.darkGray
    }
    
}
