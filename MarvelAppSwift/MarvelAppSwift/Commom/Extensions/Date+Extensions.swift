//
//  Date+Extensions.swift
//  MarvelAppSwift
//
//  Created by Matheus Lima Gomes on 21/03/20.
//  Copyright © 2020 Matheus Lima Gomes. All rights reserved.
//

import Foundation

extension Date {
    static var currentTimeStamp: Int64{
        return Int64(Date().timeIntervalSince1970 * 1000)
    }
}
