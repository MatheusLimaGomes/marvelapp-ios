//
//  UIViewController+Extensions.swift
//  MarvelAppSwift
//
//  Created by Matheus Lima Gomes on 21/03/20.
//  Copyright © 2020 Matheus Lima Gomes. All rights reserved.
//

import Foundation
import UIKit

let activityIndicator = UIActivityIndicatorView()
var messageContainer: UIView?

extension UIViewController {
    
    func startLoading() {
        DispatchQueue.main.async {
            activityIndicator.center = CGPoint(x: self.view.center.x, y: self.view.center.y)
            activityIndicator.style = .large
            self.view.addSubview(activityIndicator)
            self.view.bringSubviewToFront(activityIndicator)
            activityIndicator.startAnimating()
        }
    }
    
    func stopLoading() {
        DispatchQueue.main.async {
            activityIndicator.stopAnimating()
            activityIndicator.removeFromSuperview()
        }
    }
}
