//
//  String+Crypto.swift
//  MarvelAppSwift
//
//  Created by Matheus Lima Gomes on 21/03/20.
//  Copyright © 2020 Matheus Lima Gomes. All rights reserved.
//

import Foundation
import CryptoKit

func MD5(string: String) -> String {
    let digest = Insecure.MD5.hash(data: string.data(using: .utf8) ?? Data())
    
    return digest.map {
        String(format: "%02hhx", $0)
    }.joined()
}
