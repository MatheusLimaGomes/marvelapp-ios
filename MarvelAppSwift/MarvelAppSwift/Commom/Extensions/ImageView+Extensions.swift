//
//  ImageView+Extensions.swift
//  MarvelAppSwift
//
//  Created by Matheus Lima Gomes on 21/03/20.
//  Copyright © 2020 Matheus Lima Gomes. All rights reserved.
//

import Foundation
import UIKit

extension UIImageView {
    func makeItCircle() {
           self.layer.cornerRadius  = self.frame.height/2
           self.layer.masksToBounds = false
           self.clipsToBounds = true
           self.contentMode = .scaleAspectFit
       }
    func loadImage(withURL: URL?) {
          let transition: CATransition = CATransition.init()
          transition.duration = 1.0
          transition.timingFunction = CAMediaTimingFunction.init(name: .easeInEaseOut)
          transition.type = .fade
          self.layer.add(transition, forKey: nil)
          
          Download.downloadImage(withURL: withURL, completion: { (image, error) in
              if error == nil {
                  DispatchQueue.main.async {
                      self.image = image
                  }
              }
          })
      }
}
