//
//  MarvelApiProvider.swift
//  MarvelAppSwift
//
//  Created by Matheus Lima Gomes on 21/03/20.
//  Copyright © 2020 Matheus Lima Gomes. All rights reserved.
//

import Foundation

class MarvelApiProvider {
    static let baseURL = "http://gateway.marvel.com/v1/public/"
    static let endPointCharacters = "characters"
    static let publicKey = "ecc27327e07e98e7e00dc547015ee049"
    static let privateKey = "4e0ecbd6455df1780144a06f180671173689e7fe"

    static func getCredentials() -> String {
        let credentials = "\(baseURL)" + "\(endPointCharacters)?" + "ts=\(Date.currentTimeStamp)&" + "apikey=\(publicKey)" + "&hash\(MD5(string: privateKey))"
        return credentials
    }
    
    private static let configuration: URLSessionConfiguration = {
           let config = URLSessionConfiguration.default
           config.httpAdditionalHeaders = ["Content-Type":"application/json"]
           config.timeoutIntervalForRequest = 30.0
           return config
       }()
       
       static let session = URLSession(configuration: configuration)
}
