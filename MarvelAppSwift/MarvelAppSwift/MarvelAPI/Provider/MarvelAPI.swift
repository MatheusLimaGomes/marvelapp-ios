//
//  MarvelAPI.swift
//  MarvelAppSwift
//
//  Created by Matheus Lima Gomes on 22/03/20.
//  Copyright © 2020 Matheus Lima Gomes. All rights reserved.
//

import Foundation

class MarvelAPI: CharactersProtocol {
    func fetchCharactersList(completionHandler: @escaping CharactersCallBack) {
        guard let url = URL(string: MarvelApiProvider.getCredentials()) else {
            return
        }
        CharacterApiProvider.fetchCharacter(url: url) { (result) in
            completionHandler(result)
        }
    }
    
    
}
