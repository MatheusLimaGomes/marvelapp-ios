//
//  Characters.swift
//  MarvelAppSwift
//
//  Created by Matheus Lima Gomes on 21/03/20.
//  Copyright © 2020 Matheus Lima Gomes. All rights reserved.
//

import Foundation

struct Character: Decodable {
    let id: Int
    let name: String?
    let description: String?
    let thumbnail: String?
}
